import { useState } from 'react';
import { LoadingIcon } from '../../shared/components/LoadingIcon';
import { IssueList } from '../components/IssueList';
import { LabelPicker } from '../components/LabelPicker';
import { useIssueInfinite } from '../hooks';
import { State } from '../interfaces';


export const ListViewInfinite = () => {

  const [selectedLabels, setSelectedLabels] = useState<string[]>([]);
  const [state, setState] = useState<State>();


  const { issuesQuery } = useIssueInfinite({ state, labels: selectedLabels })

  const onLabelChanged = ( labelName: string ) => {
    ( selectedLabels.includes( labelName ) )
    ? setSelectedLabels( selectedLabels.filter( label => label !== labelName ) )
    : setSelectedLabels( [...selectedLabels, labelName] )
  }

  return (
    <div className="row mt-5">
      
      <div className="col-8">
        {
          issuesQuery.isLoading
          ? ( <LoadingIcon /> )
          : (
            <IssueList 
              issues={ issuesQuery.data?.pages.flat() || [] } //Se usa flat() para aplanar los arreglos, por ejemplo si viene se requiere algo que es de arreglos de arreglos, se aplanan arreglos para no quedar un arreglo dentro de otro
              state={ state }
              onStateChange={ (newState) => setState( newState ) }
            />
          ) 
        }

        <button className='btn btn-outline-primary mt-2'
        disabled={ !issuesQuery.hasNextPage }
          onClick={() => issuesQuery.fetchNextPage() }>
          Load more..
        </button>
       
      </div>
      
      <div className="col-4">
        <LabelPicker 
          selectedLabels={ selectedLabels }
          onChange={ (labelName) => onLabelChanged(labelName) }
        />
      </div>
    </div>
  )
}
