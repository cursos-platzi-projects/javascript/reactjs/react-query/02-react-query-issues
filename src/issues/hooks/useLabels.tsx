import { useQuery } from '@tanstack/react-query';
import { githubApi } from '../../api/githubApi';
import { sleep } from '../../helpers/sleep';
import { Label } from '../interfaces/label';

const getLabels = async():Promise<Label[]> => {

  await sleep(2);

  const { data } = await githubApi.get<Label[]>('/labels?per_labels=100', {
    headers: {
      Authorization: null
    }
  });
  return data;
}

export const useLabels = () => {

  const labelsQuery = useQuery(
    ['labels'],
    getLabels,
    {
      staleTime: 1000 * 60 * 60, // Esto es para que la data se mantenga por 1 hora
      //refetchOnWindowFocus: false //Para que no se recargue la pagina cada vez que se salga de ella
      //initialData: [], //Sirve para mostrar informacion previa en lo que carga la data - Por mientras se haga la peticion, se mostrara la data que tengas aqui.
      placeholderData: [ //Es similar al placeholderData - Esta data se va a mostrar cuando esta en modo 'fresh' (puedes probar quitando el staleTime)
        {
          id: 2192194047,
          node_id: "MDU6TGFiZWwyMTkyMTk0MDQ3",
          url: "https://api.github.com/repos/facebook/react/labels/Component:%20Flight",
          name: "Component: Flight",
          color: "c4523e",
          default: false
        },
        {
          id: 739761016,
          node_id: "MDU6TGFiZWw3Mzk3NjEwMTY=",
          url: "https://api.github.com/repos/facebook/react/labels/Component:%20Reconciler",
          name: "Component: Reconciler",
          color: "f9a798",
          default: false
        }
      ]
    }
  );

  return labelsQuery;
}