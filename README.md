# React Query - Github API Issues

1. Clonar repositorio
2. Ejecutar ``` yarn install``` o ```npm install```
3. Abrir el URL del proyecto

# Conceptos

## Estados de la data de la aplicacion:

fresh = Es la data que esta fresca, quiere decir a que no ha llegado a la caducidad (Es muy raro obtener esta data)

fetching = se refresca la informacion cada vez que hagamos la peticion al servidor

paused = Cuando se esta haciendo fetching, la peticion se esta tardando mucho o no trae los datos, se puede pausar el fetching

stale = Esto significa que la data ya esta vieja, esto pasa despues de que se haga el fetching. ReactQuery no confia en esta data y despues hacen un fetching para volver a refrescar la data

inactive = 

## Loadings

isLoading = Se usa para cuando no hay data en la cache, asi que esto solo se ejecutaria una vez, si regargas la web y hay cache esta variable no va a servir ya que hay data en la cache.

isFetching = Siempre se va a ejecutar cuando haces una peticion incluso cuando haya cache siempre se va estar ejecutando